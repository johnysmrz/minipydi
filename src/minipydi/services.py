from dataclasses import dataclass, field
from abc import ABC
from typing import AnyStr


@dataclass(frozen=True)
class AbstractService(ABC):
    def __init__(self) -> None:
        raise NotImplementedError(f'{self.__class__.__name__} cannot be instanciated')

    service_class: AnyStr
    args: list = field(default_factory=list)
    kwargs: dict = field(default_factory=dict)
    tags: tuple = field(default_factory=tuple)


@dataclass(frozen=True)
class FactoryService(AbstractService):
    factory_method: str = None


@dataclass(frozen=True)
class SingletonService(AbstractService):
    pass


@dataclass(frozen=True)
class LinkService:
    service_class: AnyStr

class Tagged(list):
    def __init__(self, tag: str) -> None:
        self.tag = tag
