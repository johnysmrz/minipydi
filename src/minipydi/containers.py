import logging
try:
    from .services import AbstractService, LinkService
except ImportError:
    from services import AbstractService, LinkService

from inspect import Parameter
from os import environ

logger = logging.getLogger('minipydi')

class ContainerConfig:
    def __init__(self) -> None:
        self._cfg = dict()

    def __getattr__(self, key):
        return self._cfg[key]
    
    def from_env(self, key, /, required: bool = False):
        value = environ.get(key)
        if required and value is None:
            raise KeyError("Required key %s not found as environment variable", key)
        if value:
            self._cfg[key] = value

    def __repr__(self) -> str:
        return f"<ContainerConfig {self._cfg}>"

class Container:
    def __init__(self) -> None:
        self.config = ContainerConfig()
        self._service_defs = list()
        self._service_instances = dict()

    def get_service(self, parameter: Parameter, /):
        for definition in self._service_defs:
            definition: AbstractService

            if parameter.annotation in self._service_instances:
                return self._service_instances[parameter.annotation]

            if definition.service_class == parameter.annotation:
                self._service_instances[parameter.annotation] = self._create_instance(definition)
                return self._service_instances[parameter.annotation]
    
    def add(self, definition: AbstractService, /):
        self._service_defs.append(definition)

    def _create_instance(self, definition: AbstractService):
        for i, arg in enumerate(definition.args):
            if isinstance(arg, LinkService):
                arg: LinkService
                linked_service = self._service_instances[arg.service_class]
                definition.args[i] = linked_service

        for key, kwarg in definition.kwargs.items():
            if isinstance(kwarg, LinkService):
                kwarg: LinkService
                linked_service = self._service_instances[kwarg.service_class]
                definition.args[key] = linked_service
        
        return definition.service_class(*definition.args, **definition.kwargs)

    def init_resources(self):
        for definition in self._service_defs:
            self._service_instances[definition.service_class] = self._create_instance(definition)

# class Container():
#     def __init__(self) -> None:
#         self._services = list()
#         self._service_instances = list()

#     # def scope(self, scopes: list):
#     #     if isinstance(scopes, list):
#     #         self._scopes = scopes
#     #     elif isinstance(scopes, str):
#     #         self._scopes.append(scopes)
#     #     else:
#     #         raise RuntimeError("Invalid scopes definition, must be list or str, %s given", type(scopes))
        
#     def get_service(self, parameter: Parameter, /):
#         # print(parameter.name)
#         # print(parameter.annotation)
#         return f"Service for {parameter.name} of type {parameter.annotation}"
    
    
#     def add(self, service: AbstractService, *args, **kwargs):
#         self._services.append([service, args, kwargs])
#         print(self._services)


    # def __init__(self) -> None:
    #     self._services_defs: list[AbstractService] = []
    #     self._config = {}
    #     self._service_instances = {}

    # def get_config(self) -> dict:
    #     return self._config

    # def add_yml_config(self, configs: list):
    #     for c in configs:
    #         with open(c) as fd:
    #             yml = yaml.load(fd, yaml.Loader)
    #             if yml is not None:
    #                 self._config.update(yml)

    # def register_service(self, service: AbstractService):
    #     self._services_defs.append(service)

    # def service_instance(self, service_class):
    #     logger.debug(f'Getting service instance {service_class.__name__}')
    #     if service_class.__name__ not in self._service_instances.keys():
    #         # let's find if we have a definition for required service_name
    #         for definition in self._services_defs:
    #             if definition.service_class == service_class:
    #                 break
    #         else:
    #             logger.error(f'Service {service_class} not found')
    #             return

    #         # mkay, we have a service definition!
    #         args = []
    #         kwargs = {}
    #         for arg in definition.args:
    #             args.append(self._eval_arg(arg))

    #         for kw_name, kwarg in definition.kwargs.items():
    #             kwargs[kw_name] = self._eval_arg(kwarg)

    #         self._service_instances[service_class.__name__] = service_class(*args, **kwargs)
    #     return self._service_instances[service_class.__name__]

    # def _eval_arg(self, arg: Any) -> Any:
    #     logger.debug(f'Evaluating argument {arg}')
    #     if type(arg) == str:
    #         if has(self._config, arg):
    #             return get(self._config, arg)
    #         else:
    #             logger.warning(f'Configuration value for {arg} not found, returning None instead')
    #             return None
    #     if isinstance(arg, LinkService):
    #         return self.service_instance(arg.service_class)
    #     logger.warning(f'Unable to evaluate argument {arg}')

    # def get_service(self, service):
    #     logger.debug(f'Getting service {service}')

    #     if service.__class__ == Tagged:
    #         service: Tagged
    #         for s in self._services_defs:
    #             s: AbstractService
    #             if service.tag in s.tags:
    #                 service.append(self.service_instance(s.service_class))

    #         return service
    #     else:
    #         for definition in self._services_defs:
    #             if isinstance(definition, FactoryService):
    #                 if definition.factory_method.__annotations__.get('return') == service:
    #                     service_instance = self.service_instance(definition.service_class)
    #                     return getattr(service_instance, definition.factory_method.__name__)()

    #             if isinstance(definition, SingletonService):
    #                 if definition.service_class == service:
    #                     return self.service_instance(definition.service_class)
    #     raise NotImplementedError(f'Service of type {service} not found')

    # def dump(self):
    #     return (self._services_defs)