
from functools import partial, wraps
import logging
from inspect import signature, Parameter

try:
    from .containers import Container
except ImportError:
    from containers import Container

logger = logging.getLogger('minipydi')


def injection_handler(f, container, args, kwargs):
    dose = list()
    kwdose = dict()

    for i, (parameter_name, parameter) in enumerate(signature(f).parameters.items()):
        # TODO finish implementation of positional only args dosing
        if parameter.kind == Parameter.POSITIONAL_ONLY:
            try:
                args[i]
            except IndexError:
                if not parameter.annotation == Parameter.empty:
                    logger.debug("Getting dose for %s", parameter_name)
                    dose[i] = container.get_service(parameter)
                else:
                    logger.warn("No positional argument value provided for <%s> but the annotation is empty", parameter_name)

        if parameter.kind in [Parameter.POSITIONAL_OR_KEYWORD, Parameter.KEYWORD_ONLY]:
            if parameter_name not in kwargs.keys():
                if not parameter.annotation == Parameter.empty:
                    logger.debug("Getting kwdose for %s", parameter_name)
                    kwdose.setdefault(parameter_name, container.get_service(parameter))

    return partial(f, *args, **kwargs)(*dose, **kwdose)


def inject(container: Container):
    def inject_decorator(f):
        @wraps(f)
        def inject_wrapper(*args, **kwargs):
            return injection_handler(f, container, args, kwargs)
        return inject_wrapper
    return inject_decorator


class MiniPyDI:
    def __init__(self) -> None:
        self.container = None

    # for now we support only the global scope
    def scope(self, container: Container):
        self.container = container

    def inject(self):
        container = self.container
        def inject_decorator(f):
            @wraps(f)
            def inject_wrapper(*args, **kwargs):
                return injection_handler(f, container, args, kwargs)
            return inject_wrapper
        return inject_decorator

# global instance
minipydi = MiniPyDI()
