from typing import Protocol, Any
from .services import AbstractService

class Container(Protocol):
    def add(self, service: AbstractService, *args, **kwargs) -> Any:
        ...
