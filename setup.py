import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="minipydi",
    version="0.1.5",
    author="Jan Smrz",
    author_email="jan-smrz@jan-smrz.cz",
    description="Minimalistic dependency injection tool",
    long_description=long_description,
    long_description_content_type="text/markdown",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.7',
    package_dir={'': 'src/minipydi'},
    packages=setuptools.find_packages(where='src/minipydi'),
)
